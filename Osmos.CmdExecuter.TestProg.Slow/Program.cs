﻿using System;
using System.Threading;

namespace Osmos.OsConsole.Test.Slow
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("message {0}", i);
                Thread.Sleep(500);
            }
        }
    }
}
