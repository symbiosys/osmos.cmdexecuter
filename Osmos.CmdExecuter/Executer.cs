﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Osmos.CmdExecuter
{
    public class ExecuterOptions {
        public string FilePath { get; set; }
        public string WorkingDirectory { get; set; }
        public string Arguments { get; set; }
        public ILoggingService LoggingService { get; set; }
    }

    public class Executer
    {
        string _fileName, _filePath, _workingDirectory, _arguments;

        Process _process;

        List<string> _outputs, _errors;

        ILoggingService _loggingService;

        bool _isSuccess = false;
        Exception _exception = null;

        public static Executer Create(ExecuterOptions options)
        {
            if (options == null) throw new ArgumentNullException("Options can not be null.");

            if (!File.Exists(options.FilePath))
            {
                throw new FileNotFoundException(string.Format("Can not find \"{0}\"", options.FilePath));   
            }

            var cmdExecuter = new Executer();

            cmdExecuter._filePath = options.FilePath;
            cmdExecuter._fileName = Path.GetFileName(options.FilePath);

            string fileDirectory = Path.GetDirectoryName(options.FilePath);
            if (options.WorkingDirectory != null) cmdExecuter._workingDirectory = options.WorkingDirectory;
            else cmdExecuter._workingDirectory = fileDirectory;

            cmdExecuter._arguments = options.Arguments;

            cmdExecuter._loggingService = options.LoggingService;

            return cmdExecuter;
        }

        public bool IsSuccess
        {
            get
            {
                return _isSuccess;
            }
        }

        public List<string> Outputs
        {
            get
            {
                return _outputs;
            }
        }

        public List<string> Errors
        {
            get
            {
                return _errors;
            }
        }

        public Exception Exception
        {
            get
            {
                return _exception;
            }
        }

        private void _LogStart()
        {
            if (_loggingService == null) return;
            _loggingService.Log(string.Format("Started executing \"{0}\"", _fileName));
            if (!string.IsNullOrEmpty(_workingDirectory))
                _loggingService.Log(string.Format("Working directory : \"{0}\"", _workingDirectory));
            if (!string.IsNullOrEmpty(_arguments))
                _loggingService.Log(string.Format("Arguments : \"{0}\"", _arguments));
        }

        private void _LogOutput(string output)
        {
            if (_loggingService == null) return;
            _loggingService.Log(string.Format("Output : {0}", output));
        }

        private void _LogError(string error)
        {
            if (_loggingService == null) return;
            _loggingService.Log(string.Format("Error : {0}", error));
        }

        private void _LogSuccess()
        {
            if (_loggingService == null) return;
            _loggingService.Log(string.Format("Executing \"{0}\" success", _fileName));
        }

        private void _LogException(Exception e)
        {
            if (_loggingService == null) return;
            _loggingService.Log(string.Format("Executing \"{0}\" error", _fileName));
            _loggingService.Log(string.Format("Exception type {0}", e.GetType()));
            _loggingService.Log(string.Format("Exception message {0}", e.Message));
        }

        private void _LogEnd()
        {
            if (_loggingService == null) return;
            _loggingService.Log(string.Format("Finished executing \"{0}\"", _fileName));
            if(_process != null && _process.HasExited)
                _loggingService.Log(string.Format("Total time :  {0} (ms)", _process.TotalProcessorTime.TotalMilliseconds));
        }

        private void _ProcessExited()
        {
            _LogEnd();
            int exitCode = 0;
            
            if (_process != null)
            {
                exitCode = _process.ExitCode;
                _process.Close();
                _process.Dispose();
                _process = null;
            }

            _isSuccess = exitCode == 0;
            if (_isSuccess) _LogSuccess();
            else _LogError(string.Format("The process \"{0}\" failed", _fileName));
        }

        public async Task TryExecuteAsync() {
            _process = new Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WorkingDirectory = _workingDirectory;
            startInfo.FileName = _filePath;
            startInfo.Arguments = _arguments;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            _process.StartInfo = startInfo;

            _LogStart();

            try
            {
                if (_outputs == null) _outputs = new List<string>();
                else _outputs.Clear();
                if (_errors == null) _errors = new List<string>();
                else _errors.Clear();

                _process.Start();
                using (StreamReader sr = _process.StandardOutput)
                {
                    string output;
                    while ((output = await sr.ReadLineAsync()) != null)
                    {
                        if (output != null)
                        {
                            _outputs.Add(output);
                            _LogOutput(output);
                        }
                    }
                }
                using (StreamReader sr = _process.StandardError)
                {
                    string error;
                    do
                    {
                        error = await sr.ReadLineAsync();
                        if (!string.IsNullOrEmpty(error))
                        {
                            _errors.Add(error);
                            _LogError(error);
                        }
                        while ((error = await sr.ReadLineAsync()) != null)
                        {
                            if (error != null)
                            {
                                _errors.Add(error);
                                _LogError(error);
                            }
                        }
                    } while (!string.IsNullOrEmpty(error));
                }
                _process.WaitForExit();
            }
            catch (Exception e)
            {
                _exception = e;
                _LogException(e);
            }
            finally {
                _ProcessExited();
            }
        }
    }
}
