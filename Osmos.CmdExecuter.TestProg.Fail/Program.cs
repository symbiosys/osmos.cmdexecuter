﻿using System;

namespace Osmos.OsConsole.Test.Fail
{
    class Program
    {
        static void Main(string[] args)
        {
            throw new Exception("This is an exception from a console app.");
        }
    }
}
