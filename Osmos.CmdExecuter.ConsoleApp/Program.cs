﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osmos.CmdExecuter.ConsoleApp
{
    class LoggingService : ILoggingService
    {
        public void Log(string message)
        {
            System.Console.WriteLine(message);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string solutionPath = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName).FullName;
            string filePath = Path.Combine(solutionPath, @"Osmos.CmdExecuter.TestProg.Slow\bin\Debug\Osmos.CmdExecuter.TestProg.Slow.exe");

            var loggingService = new LoggingService();
            var options = new ExecuterOptions{
                FilePath = filePath,
                LoggingService = loggingService
            };
            var executer = Executer.Create(options);

            var task = Task.Run(async () => await executer.TryExecuteAsync());
            task.Wait();
        }
    }
}
