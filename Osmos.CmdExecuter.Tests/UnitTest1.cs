﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Threading.Tasks;

namespace Osmos.CmdExecuter.Tests
{
    class LoggingService : ILoggingService
    {
        public void Log(string message)
        {
            System.Console.WriteLine(message);
        }
    }

    [TestClass]
    public class UnitTest1
    {
        ILoggingService _loggingService;

        string _solutionPath;

        public UnitTest1()
        {
            _loggingService = new LoggingService();
            _solutionPath = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName).FullName;
        }

        [TestMethod]
        public void Empty()
        {
            System.Console.WriteLine("Empty");
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void create_fails_for_bad_filepath()
        {
            var options = new ExecuterOptions
            {
                FilePath = "wrong_filepath.exe",
                LoggingService = _loggingService
            };
            var executer = Executer.Create(options);
        }

        [TestMethod]
        public void create_succeed_for_good_filepath()
        {
            string filePath = Path.Combine(_solutionPath,
@"Osmos.CmdExecuter.TestProg.Slow\bin\Debug\Osmos.CmdExecuter.TestProg.Slow.exe");
            var options = new ExecuterOptions
            {
                FilePath = filePath,
                LoggingService = _loggingService
            };
            var executer = Executer.Create(options);

            Assert.IsNull(executer.Exception);
        }

        [TestMethod]
        public async Task tryexecute_success()
        {
            string filePath = Path.Combine(_solutionPath,
@"Osmos.CmdExecuter.TestProg.Slow\bin\Debug\Osmos.CmdExecuter.TestProg.Slow.exe");
            var options = new ExecuterOptions
            {
                FilePath = filePath,
                LoggingService = _loggingService
            };
            var executer = Executer.Create(options);

            await executer.TryExecuteAsync();

            Assert.IsTrue(executer.IsSuccess);
            Assert.IsNull(executer.Exception);
        }

        [TestMethod]
        public async Task tryexecute_error()
        {
            string filePath = Path.Combine(_solutionPath,
@"Osmos.CmdExecuter.TestProg.Fail\bin\Debug\Osmos.CmdExecuter.TestProg.Fail.exe");
            var options = new ExecuterOptions
            {
                FilePath = filePath,
                LoggingService = _loggingService
            };
            var executer = Executer.Create(options);

            await executer.TryExecuteAsync();

            Assert.IsFalse(executer.IsSuccess);
        }
    }
}
